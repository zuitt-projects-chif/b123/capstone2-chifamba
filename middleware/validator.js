const constants = require('../constants');

/**
 * Checks req.body for to see if listed fields were provided
 * before moving on to next middleware. Sends a 422 if any fields are missing
 */
module.exports.validateBodyMiddleware = (requiredFields) => (req, res, next) => {
  if (Array.isArray(requiredFields)) {
    const errors = [];
    for (const field of requiredFields) {
      if (!req.body[field]) {
        errors.push(`${field} is required`);
      }
    }
    if (errors.length > 0) {
      return res.status(422).send({
        message: constants.messages.MISSING_REQUIRED_FIELDS,
        errors,
      });
    }
    next();
  } else {
    console.warn(
      'Data passed to validateBodyFields was not array. No validation was done!',
    );
    next();
  }
};

/**
 * Checks req.body for to see if listed fields were provided
 * before handing over control to controller. Sends a 422 if any fields are missing
 */
module.exports.withValidateBody = (requiredFields) => (callback) => (req, res, next) => {
  const errors = [];
  for (const field of requiredFields) {
    if (!req.body[field]) {
      errors.push(`${field} is required`);
    }
  }
  if (errors.length > 0) {
    return res.status(422).send({
      message: constants.messages.MISSING_REQUIRED_FIELDS,
      errors,
    });
  }
  return callback(req, res, next);
};
