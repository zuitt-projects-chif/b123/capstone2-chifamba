/**
 * Contains the bulk of authentication logic and token management
 */

const jwt = require('jsonwebtoken');
const constants = require('../constants');
const config = require('../config');

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(data, config.JWT_SECRET, {});
};

module.exports.verifyAuth = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token === 'undefined') {
    return res.status(401).send({message: constants.messages.AUTH_FAIL_NO_TOKEN});
  }
  token = token.slice(7, token.length);
  jwt.verify(token, config.JWT_SECRET, (err, decodedToken) => {
    if (err) {
      return res.status(401).send({
        message: constants.messages.AUTH_FAIL_INVALID_TOKEN,
        details: err.message,
      });
    }
    req.user = decodedToken;
    next();
  });
};

module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.status(403).send({
      message: constants.messages.AUTH_FAIL_NO_PERMISSION,
    });
  }
};

module.exports.attachUserIfPresent = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token === 'undefined') {
    next();
  } else {
    token = token.slice(7, token.length);
    jwt.verify(token, config.JWT_SECRET, (err, decodedToken) => {
      if (!err) {
        req.user = decodedToken;
      }
      next();
    });
  }
};
