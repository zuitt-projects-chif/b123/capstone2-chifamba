const {version} = require('./package.json');
const express = require('express');
const morgan = require('morgan');
const listEndpoints = require('express-list-endpoints');
const {db} = require('./services/db');
const authRoutes = require('./routes/authRoutes');
const usersRoutes = require('./routes/usersRoutes');
const productsRoutes = require('./routes/productsRoutes');
const ordersRoutes = require('./routes/ordersRoutes');
const cors = require('cors');

// log db events to console
db.on('error', console.error.bind(console, 'DB Connection Error.'));
db.once('open', () =>
  console.log(`${new Date().toLocaleTimeString()}| Connected to MongoDB`),
);

// init express
const app = express();
const port = process.env.PORT || 4000;

// express middleware
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

// express routes
app.use('/auth', authRoutes);
app.use('/users', usersRoutes);
app.use('/products', productsRoutes);
app.use('/orders', ordersRoutes);

app.get('/', (req, res) =>
  res.send({
    status: 'Running',
    version,
    endpoints: listEndpoints(app)
      .flatMap((entry) =>
        entry.methods.map((method) => {
          return `[${method}] ${req.protocol}://${req.hostname}${
            port === 4000 ? ':' + port : ''
          }${entry.path}`;
        }),
      )
      .sort(),
  }),
);

// start express
app.listen(port, () =>
  console.log(`${new Date().toLocaleTimeString()}| Server is running on ${port}`),
);
