# Chellow (E-Commerce API)

### Live Demo:

https://chellow.herokuapp.com/

## Setting Up Development Environment

#### 1. Cloning

Requirements: Git (https://git-scm.com/downloads)

Clone the repository to your local machine by entering the following in your cmd or terminal

```
git clone https://gitlab.com/zuitt-projects-chif/b123/capstone2-chifamba.git
```

#### 2. Installing Dependencies

Requirements: Node/NPM (https://nodejs.org/en/download/)

Install the dependencies locally using

```
npm install
```

#### 3. Setting Environment Variables

Create a `.env` file in the root folder. Define the following

```
MONGODB_USERNAME=your_username
MONGODB_PASSWORD=your_db_password
MONGODB_DATABASE_HOST=your_database_host/your_db_name
JWT_SECRET=a_random_secret
```

`MONGODB_USERNAME` - this is the user name for your MongoDB. see https://docs.atlas.mongodb.com/tutorial/create-mongodb-user-for-cluster/

`MONGODB_PASSWORD` - this is the password for the database. see https://docs.atlas.mongodb.com/tutorial/create-mongodb-user-for-cluster/

`MONGODB_DATABASE_HOST` - this is the hostname or IP address and db name. Like `<your_database_host>/<your_db_name>`

#### 4. Starting development server

Start the development server using the following in your terminal:

```
npm run dev
```

Development server will start on http://localhost:3000

## Running Production Version

You can run the server using:

```
npm start
```

### Features:

- User registration
- User authentication
- Set user as admin (Admin only)
- Create Product
- Retrieve All Active Products
- Retrieve All Products (Admin only)
- Update Product
- Archive Product (Admin only)
- Search Products
- Get Products By Category
- Create Order
- Update Order Status (Admin only)
- Get authenticated user's orders
- Get All Orders

---

#### Registration

**POST** - http://localhost:4000/users

Body (JSON):

```
{
    "firstName": String,
    "lastName": String,
    "email": String,
    "password": String,
    "mobileNo": String,
}
```

---

#### Login

**POST** - http://localhost:4000/auth

Body (JSON):

```
{
    "email": String,
    "password": String
}
```

---

#### Set As Admin

**PUT** - http://localhost:4000/users/addAdmin/{id}

Body: `No Request Body`

_Admin Token Required_

---

#### Set as regular user

**PUT** - http://localhost:4000/users/removeAdmin/{id}

Body: `No Request Body`

_Admin Token Required_

---

#### Create Product

**POST** - http://localhost:4000/products

Body (JSON):

```
{
    "name": String,
    "description": String,
    "price": Number,
    "images"?: String[],
    "category"?: String,
}
```

---

#### Retrieve All Active Products

**GET** - http://localhost:4000/products

Apply Filters:

**GET** - http://localhost:4000/products?{filter}={value}

Supported filters: `minPrice`, `maxPrice`, `category`

---

#### Retrieve All Products (including inactive)

**GET** - http://localhost:4000/products/all

_Admin Token Required_

---

#### Retrieve Single Product

**GET** - http://localhost:4000/product/{id}

---

#### Update Product

**PUT** - http://localhost:4000/products

Body (JSON):

```
{
    "name": String,
    "description": String,
    "price": Number,
    "images"?: String[],
    "category"?: String,
}
```

_Admin Token Required_

---

#### Archive Product (Soft Delete)

**PUT** - http://localhost:4000/products/archive/{id}

Body (JSON): `No request body`

_Admin Token Required_

---

#### Activate Product

**PUT** - http://localhost:4000/products/activate/{id}

Body (JSON): `No request body`

_Admin Token Required_

---

#### Search Products

**GET** - http://localhost:4000/products/search/{text}

Apply Filters:

**GET** - http://localhost:4000/products/search/{text}?filter={value}

Supported filters: `minPrice`, `maxPrice`, `category`

---

#### Get Products in Category

**GET** - http://localhost:4000/products/category/{category}

---

#### Create Order

**POST** - http://localhost:4000/orders

Body (JSON):

```
{
    totalAmount?: Number,
    products: [
        {
            id: String,
            quantity: Number
        },
        {
            id: String,
            quantity: Number
        },
        {
            id: String,
            quantity: Number
        }
        ...
    ]
}
```

---

#### Update Order Status

**PUT** - http://localhost:4000/orders/setStatus/{id}

Body (JSON):

```
{
    status: String*
}
```

\*Must be `processing`, `shipping`, `completed` or `cancelled`

_Admin Token Required_

---

#### Retrieve Authenticated User's orders

**GET** - http://localhost:4000/orders

---

#### Retrieve All Orders

**GET** - http://localhost:4000/orders

_Admin Token Required_

---

## Admin Credentials:

email: `adminAPI@gmail.com`

password: `adminAPI123`

```
{
    "email": "adminAPI@gmail.com",
    "password": "adminAPI123"
}
```
