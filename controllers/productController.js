const constants = require('../constants');
const Product = require('../models/Product');
const {withValidateBody} = require('../middleware/validator');
const {
  mongoErrorToStatus,
  mongoErrorToMessage,
  paramToMongoCriteria,
} = require('../services/db');
const supportedFilterParams = ['minPrice', 'maxPrice', 'category'];
/**
 * Creates a new product with fields name, description, price
 */
module.exports.createProduct = withValidateBody(['name', 'description', 'price'])(
  (req, res) => {
    const {name, description, price, images, category} = req.body;
    const updates = {
      name,
      description,
      price,
    };
    if (images) {
      if (!Array.isArray(images)) {
        return res
          .status(422)
          .send({message: constants.messages.IMAGES_MUST_BE_AN_ARRAY});
      }
      updates.images = images;
    }
    if (category) updates.category = category;

    const doc = new Product(updates);
    doc
      .save()
      .then((savedDoc) => res.status(201).send(savedDoc))
      .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
  },
);

/**
 * Gets all active products
 */
module.exports.getAllActiveProducts = (req, res) => {
  const criteria = {isActive: true};
  const queryParams = Object.keys(req.query);
  const supportedQueryParams = queryParams.filter((param) =>
    supportedFilterParams.includes(param),
  );
  for (const param of supportedQueryParams) {
    const mongoQueryData = paramToMongoCriteria(param, req.query[param]);
    criteria[mongoQueryData.key] = mongoQueryData.value;
  }

  Product.find(criteria)
    .then((result) => res.send(result))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Gets all products
 */
module.exports.getAllProducts = (req, res) => {
  Product.find({})
    .then((result) => res.send(result))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Gets a single product by id (from URL params)
 */
module.exports.getProductById = (req, res) => {
  Product.findById(req.params.id)
    .then((result) =>
      result
        ? res.send(result)
        : res.status(404).send({message: constants.messages.COULD_NOT_FIND_PRODUCT}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Update a single product by id (from URL params)
 */
module.exports.updateProductById = (req, res) => {
  const {name, description, price, images, category} = req.body;
  const updates = {
    name,
    description,
    price,
  };
  if (images) {
    if (!Array.isArray(images)) {
      return res.status(422).send({message: constants.messages.IMAGES_MUST_BE_AN_ARRAY});
    }
    updates.images = images;
  }
  if (category) updates.category = category;

  Product.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then((result) => res.send(result))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Archive a product by id (from URL params). Sets isActive to false
 */
module.exports.archiveProductById = (req, res) => {
  const updates = {
    isActive: false,
  };
  Product.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then((result) =>
      res.send({message: `${result.name} ${constants.messages.ARCHIVED}`}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Unarchive a product by id (from URL params). Sets isActive to true
 */
module.exports.activateProductById = (req, res) => {
  const updates = {
    isActive: true,
  };
  Product.findByIdAndUpdate(req.params.id, updates, {new: true})
    .then((result) =>
      res.send({message: `${result.name} ${constants.messages.ACTIVATED}`}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Search for a product by text (from URL params). The search is not case sensitive
 */
module.exports.searchProducts = (req, res) => {
  const isAdmin = req.user && req.user.isAdmin;
  const criteria = isAdmin
    ? {name: {$regex: req.params.text, $options: '$i'}}
    : {isActive: true, name: {$regex: req.params.text, $options: '$i'}};

  const queryParams = Object.keys(req.query);
  const supportedQueryParams = queryParams.filter((param) =>
    supportedFilterParams.includes(param),
  );
  for (const param of supportedQueryParams) {
    const mongoQueryData = paramToMongoCriteria(param, req.query[param]);
    criteria[mongoQueryData.key] = mongoQueryData.value;
  }

  Product.find(criteria)
    .then((result) =>
      result && result.length > 0 ? res.send(result) : res.status(404).send(result),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Get products in a category. The category matching is not case sensitive
 */
module.exports.getCategory = (req, res) => {
  const isAdmin = req.user && req.user.isAdmin;
  const criteria = isAdmin
    ? {category: {$regex: req.params.categoryName, $options: '$i'}}
    : {
        isActive: true,
        category: {$regex: `\\b${req.params.categoryName}\\b`, $options: '$i'},
      };
  Product.find(criteria)
    .then((result) =>
      result && result.length > 0 ? res.send(result) : res.status(404).send(result),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};
