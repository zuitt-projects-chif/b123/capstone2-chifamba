const constants = require('../constants');
const Order = require('../models/Order');
const Product = require('../models/Product');
const {mongoErrorToStatus, mongoErrorToMessage} = require('../services/db');

/**
 * Creates a new order from products[]
 */
module.exports.createOrder = async (req, res) => {
  // only regular users can create order
  if (req.user.isAdmin) {
    return res.status(403).send({
      message: constants.messages.FORBIDDEN_ONLY_REGULAR_USERS,
    });
  }

  // products to be in {id: <id>, quantity: <quantity>} format
  const {products} = req.body;
  if (!products || !Array.isArray(products) || products.length < 1) {
    return res.status(422).send({
      message: constants.messages.LIST_OF_PRODUCTS_IS_REQUIRED,
    });
  } else {
    // validate product items
    const hasValidationErrors = products.find((product) => {
      if (!product.id || !product.quantity) {
        return true;
      }
    });
    if (hasValidationErrors) {
      return res.status(422).send({
        message: constants.messages.ORDER_ITEMS_MUST_HAVE,
      });
    }
  }

  // Retrieve products, to get name and price detail. (For security, cannot trust client for these)
  let productDocs = [];
  try {
    const ids = products.map((product) => product.id);
    productDocs = await Product.find({_id: {$in: ids}, isActive: true});
  } catch (e) {
    return res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e));
  }

  const orderItems = products.map((product) => {
    const actualProduct = productDocs.find((p) => p.id === product.id);
    const pObj = {
      productId: product.id,
      name: actualProduct.name,
      price: actualProduct.price,
      quantity: product.quantity,
    };
    if (actualProduct.images[0]) {
      pObj.image = actualProduct.images[0];
    }
    return pObj;
  });

  const totalAmount = orderItems.reduce((accumulatedTotal, currentOrderItem) => {
    return accumulatedTotal + currentOrderItem.price * currentOrderItem.quantity;
  }, 0);

  /* 
  Check if the client provided a totalAmount. 
  If there is a totalAmount provided by client, check it against the backend totalAmount.
      If there is a mismatch, return an error.
  If there is no totalAmount provided, use the one computed by server here;
  */
  const clientTotalAmount = req.body.totalAmount;
  if (clientTotalAmount && clientTotalAmount !== totalAmount) {
    return res.status(422).send({
      message: constants.messages.CLIENT_TOTAL_AMOUNT_MISMATCH,
      details: {
        received: clientTotalAmount,
        expected: totalAmount,
      },
    });
  }

  console.log('$$$ Total (-: ', totalAmount);
  const doc = new Order({
    userId: req.user.id,
    totalAmount,
    products: orderItems,
  });
  doc
    .save()
    .then((savedDoc) => res.status(201).send(savedDoc))
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Gets all orders. Admin gets all system order, user gets only their order
 */
module.exports.getAllOrders = (req, res) => {
  const isAdmin = req.user && req.user.isAdmin;
  const criteria = isAdmin ? {} : {userId: req.user.id};
  Order.find(criteria)
    .populate('userId', {_id: 1, firstName: 1, lastName: 1})
    .then((result) => {
      return res.send(result);
    })
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Gets a single order by id (from URL params). Must be Admin or owner of order
 */
module.exports.getOrderById = (req, res) => {
  const isAdmin = req.user && req.user.isAdmin;
  const criteria = isAdmin
    ? {_id: req.params.id}
    : {_id: req.params.id, userId: req.user.id};
  Order.findOne(criteria)
    .populate('userId', {password: 0, isAdmin: 0})
    .then((result) =>
      result
        ? res.send(result)
        : res.status(404).send({message: constants.messages.COULD_NOT_FIND_ORDER}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};

/**
 * Updates a single order by id (from URL params). Must be Admin
 */
module.exports.updateOrderStatusById = (req, res) => {
  // findByIdAndUpdate() will not trigger enum validation.
  Order.findOne({_id: req.params.id})
    .then((order) => {
      order.status = req.body.status;
      order.save();
      return order;
    })
    .then((result) =>
      result
        ? res.send({
            message: constants.messages.ORDER_STATUS_UPDATED,
            status: result.status,
          })
        : res.status(404).send({message: constants.messages.COULD_NOT_FIND_ORDER}),
    )
    .catch((e) => res.status(mongoErrorToStatus(e)).send(mongoErrorToMessage(e)));
};
