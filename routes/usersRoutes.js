/**
 * Contains routes for /users
 */

const express = require('express');
const router = express.Router();
const {
  createUser,
  getAllUsers,
  getUserById,
  getUserProfile,
  updateUserProfile,
  addAdmin,
  removeAdmin,
} = require('../controllers/userController');

const {verifyAuth, verifyAdmin} = require('../middleware/auth');

router.post('/', createUser);

router.get('/', verifyAuth, verifyAdmin, getAllUsers);

router.get('/me', verifyAuth, getUserProfile);

router.put('/me', verifyAuth, updateUserProfile);

router.get('/:id', verifyAuth, verifyAdmin, getUserById);

router.put('/addAdmin/:id', verifyAuth, verifyAdmin, addAdmin);

router.put('/removeAdmin/:id', verifyAuth, verifyAdmin, removeAdmin);

module.exports = router;
