const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User Id is required'],
  },
  totalAmount: {
    type: Number,
    required: [true, 'Total amount is required '],
  },
  datePurchased: {
    type: Date,
    default: new Date(),
  },
  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product',
        required: [true, 'Product Id is required'],
      },
      name: {
        type: String,
        required: [true, 'Name is required'],
      },
      price: {
        type: Number,
        required: [true, 'Price is required '],
      },
      quantity: {
        type: Number,
        required: [true, 'Quantity is required '],
        min: [1, 'You must have at least 1 item'],
      },
      image: {
        type: String,
        default: null,
      },
    },
  ],
  status: {
    type: String,
    enum: ['processing', 'shipping', 'completed', 'cancelled'],
    default: 'processing',
  },
});

module.exports = mongoose.model('Order', orderSchema);
