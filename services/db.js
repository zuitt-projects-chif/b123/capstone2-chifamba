/**
 * Contains bulk of Database related fns and utilities
 */

const config = require('../config');
const constants = require('../constants');
const mongoose = require('mongoose');

mongoose.connect(
  `mongodb+srv://${config.MONGODB_USERNAME}:${config.MONGODB_PASSWORD}@${config.MONGODB_DATABASE_HOST}?retryWrites=true&w=majority`,
  {useNewUrlParser: true, useUnifiedTopology: true},
);

module.exports.db = mongoose.connection;

const mongoErrorToApiStatusOrMessage = (error, messageMode) => {
  console.error(
    error.code ? error.code : '',
    error.name ? error.name : '',
    error.message ? error.message : '',
  );
  if (error.code) {
    switch (error.code) {
      case 11000:
        const dupKey = Object.keys(error.keyPattern)[0];
        return messageMode
          ? {
              message: `${dupKey} already exists`,
              details: error.message ? error.message : '',
            }
          : 409;
      default:
        return messageMode ? {message: constants.messages.AN_ERROR_OCCURED} : 500;
    }
  } else if (error.name) {
    switch (error.name) {
      case 'CastError':
        return messageMode
          ? {
              message: constants.messages.INVALID_REQUEST_ARGUMENTS,
              details: error.message ? error.message : '',
            }
          : 400;
      case 'ValidationError':
        return messageMode
          ? {
              message: constants.messages.INVALID_REQUEST_ARGUMENTS,
              details: error.message ? error.message : '',
            }
          : 422;
      default:
        return messageMode ? {message: constants.messages.AN_ERROR_OCCURED} : 500;
    }
  } else {
    return messageMode ? {message: constants.messages.AN_ERROR_OCCURED} : 500;
  }
};
module.exports.mongoErrorToStatus = (e) => mongoErrorToApiStatusOrMessage(e);
module.exports.mongoErrorToMessage = (e) => mongoErrorToApiStatusOrMessage(e, true);

module.exports.paramToMongoCriteria = (queryParam, value) => {
  switch (queryParam) {
    case 'minPrice':
      return {key: 'price', value: {$gt: value}};
    case 'maxPrice':
      return {key: 'price', value: {$lt: value}};
    default:
      return {key: queryParam, value};
  }
};
