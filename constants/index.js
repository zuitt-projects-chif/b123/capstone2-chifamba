/**
 * Contains shared constants like messages, formulas, etc
 */

const messages = require('./messages');

module.exports = {messages};
