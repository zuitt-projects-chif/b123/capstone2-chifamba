// create a .env file
require('dotenv').config();

if (
  !process.env.MONGODB_USERNAME ||
  !process.env.MONGODB_PASSWORD ||
  !process.env.MONGODB_DATABASE_HOST ||
  !process.env.JWT_SECRET
) {
  console.error(
    `process.env.MONGODB_USERNAME, process.env.MONGODB_PASSWORD, process.env.MONGODB_DATABASE_HOST, process.env.JWT_SECRET must all be defined. If running on local server, create a .env file in the root folder and define them in that file like: 
    
MONGODB_USERNAME=username
MONGODB_PASSWORD=password
MONGODB_DATABASE_HOST=cluster_path/database_name
JWT_SECRET=a_random_secret
`,
  );

  process.exit(1);
}
module.exports = {
  MONGODB_USERNAME: process.env.MONGODB_USERNAME,
  MONGODB_PASSWORD: process.env.MONGODB_PASSWORD,
  MONGODB_DATABASE_HOST: process.env.MONGODB_DATABASE_HOST,
  JWT_SECRET: process.env.JWT_SECRET,
};
